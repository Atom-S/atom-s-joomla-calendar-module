<?php
// No direct access
defined( '_JEXEC' ) or die;

?>
<div class="module<?php echo $moduleclass_sfx; ?>">
    <?php if( !empty($headline) ): ?><div class="m-calendaratoms-heading <?php echo $params->get( 'headline_class', '' ); ?>"><?php echo $headline ?></div><?php endif; ?>
    <?php if( !empty($preHeading) ): ?><div class="m-calendaratoms-preeading"><?php echo $preHeading ?></div><?php endif; ?>
    <?php if( !empty($desc) ): ?><div class="m-calendaratoms-desc"><?php echo $desc ?></div><?php endif; ?>
    <?php if( isset($list) && !empty($list) ): ?>
        <div class="container-caption">
            <span id="prev" class="cycle-arrow"><a rel="nofollow" href="javascript:void(0);"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></span> 
            <span id="custom-caption" class="center"></span>
            <span id="next" class="cycle-arrow"><a rel="nofollow" href="javascript:void(0);"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></span>
        </div>
        <div style="clear:both;"></div>
        <div class="cycle-slideshow" data-cycle-fx="fade" data-cycle-timeout="0" data-cycle-slides="> div" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-speed="1" data-cycle-log="false" data-allow-wrap="false">
            <?php foreach( $list as $dates ): ?>
                <div style="display:none;">
                    <div class="boxes">
                    <?php foreach($dates as $date => $tours): ?>
                            <div class="box box1">
                                <div class="calendar-row">
                                    <div class="day-week"><?php echo JText::_(modCalendarAtomsHelper::$datWeek[date('N', strtotime($date))]) . ' ' . $date; ?></div>                            
                                    <div class="calendar-tour">
                                        <?php if( !empty($tours) ): ?>
                                            <?php foreach($tours as $time => $tour): ?>
                                                <?php for($i = 0; $i < count($tour); $i++): ?>
                                                    <span class="calendar-tour-time"><?php echo $time; ?></span>
                                                    <br />
                                                    <span class="calendar-tour-link"><a href="<?php echo modCalendarAtomsHelper::createLink($tour[$i]['alias'], array('tour'), array('parent', $showcaseKey), '&date='.date('d.m.y', strtotime($date)).'&time='.$time ); ?>"><?php echo $tour[$i]['name']; ?></a></span>
                                                    <br />
                                                <?php endfor; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <span class="calendar-tour-no-events"><?php echo JText::_('MOD_CALENDARATOMS_NO_EVENTS') ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                    <?php endforeach; ?> 
                    </div>        
                </div>
            <?php endforeach; ?>      
        </div>
        <div class="cycle-slideshow content" data-cycle-fx="fade" data-cycle-timeout="0" data-cycle-slides="> div" data-cycle-caption="#custom-caption" data-cycle-caption-template="{{cyclePeriod}}" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-speed="1" data-cycle-log="false" data-allow-wrap="false">
            <?php foreach( $list as $tour ): ?>
            <div style="display:none;" data-cycle-period="<?php $dates = array_keys($tour); ?><span class='date-period'><?php echo $dates[0]; ?></span> <i class='fa fa-minus' aria-hidden='true'></i> <span class='date-period'><?php echo $dates[6]; ?></span>"></div>
            <?php endforeach; ?>  
        </div>
    <?php else: ?>
        <h3><?php echo JText::_('MOD_CALENDARATOMS_SHOWCASE_NOT_FOUND') ?></h3>
    <?php endif; ?>
    <div style="clear: both;"></div>
</div>