<?php

// No direct access
defined( '_JEXEC' ) or die;

require_once( dirname( __FILE__ ) . '/helper.php' );

$headline = $params->get('headline', '');
$preHeading = $params->get('pre_heading', '');
$desc = $params->get('desc', '');
$cssHeadlineClass = $params->get('css_headline_class', '');
$cssDescClass = $params->get('css_description_class', '');
$showcaseKey = $params->get('showcase_key', '');

$datesObj = new stdClass();
$datesObj = modCalendarAtomsHelper::getToursEvents($params, $showcaseKey);
$list = array();    
if( $datesObj != new stdClass() ) {
    JHtml::_('jquery.framework', true);
    $doc = JFactory::getDocument();
    $doc->addStyleSheet( JUri::base() . 'media/' . $module->module . '/styles/styles.css' );
    // division into parts of an array
    $list = array_chunk($datesObj->tours, 7, true);
    modCalendarAtomsHelper::addScripts($params, $module);
} 

$moduleclass_sfx = htmlspecialchars( $params->get( 'moduleclass_sfx' ) );
require( JModuleHelper::getLayoutPath( 'mod_calendaratoms', $params->get( 'layout', 'default' ) ) );