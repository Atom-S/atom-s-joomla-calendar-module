<?php

// No direct access
defined( '_JEXEC' ) or die;

/**
 * Class Module Helper
 * @author Korotkov Vadim
 */
class modCalendarAtomsHelper
{
    
    /**
     * 
     * Prefix API url atom-s system
     * 
     * @var string
     * @since  1.0.0
     * 
     */
    static $prefixApiUrl = "api/";
    
    /**
     * 
     * API url atom-s system
     * 
     * @var string
     * @since  1.0.0
     * 
     */
    static $apiUrl = "https://atom-s-alvoro.c9users.io/api/";
    
    /**
     * 
     * API url for list tours in atom-s system
     * 
     * @var string
     * @since  1.0.0
     * 
     */
    static $apiShowcaseUrl = "v{api_version}/{api_key}/search.json";

    /**
     * 
     * Format date for sort func
     * 
     * @var string
     * @since  1.0.0
     * 
     */
    static private $_format = '';
    
    /**
     * 
     * Date weeks
     * 
     * @var string
     * @since  1.0.0
     * 
     */
    static public $datWeek = array(
        '1' => 'MOD_CALENDARATOMS_DAY_WEEK_NODAY_ABB',
        '2' => 'MOD_CALENDARATOMS_DAY_WEEK_TUESDAY_ABB',
        '3' => 'MOD_CALENDARATOMS_DAY_WEEK_WEDNESDAY_ABB',
        '4' => 'MOD_CALENDARATOMS_DAY_WEEK_THURSDAY_ABB',
        '5' => 'MOD_CALENDARATOMS_DAY_WEEK_FRIDAY_ABB',
        '6' => 'MOD_CALENDARATOMS_DAY_WEEK_SATURDAY_ABB',
        '7' => 'MOD_CALENDARATOMS_DAY_WEEK_SUNDAY_ABB',
    );
    
    /**
     * 
     * The method get in json for showcase
     * 
     * @return object
     * 
     */
    static function getToursEvents( $params = array(), $showcaseKey = '' ) {
        
        $data = new stdClass();
        
        try 
        {
            
            // get params mwnu item
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
    		$query
                ->select('*')
                ->from('#__menu')
                ->where('link = \'index.php?option=com_atoms&view=showcase&key=' . $showcaseKey . '\'');
            $issetMenuItem = $db->setQuery($query)->loadObject();
            $query->clear();
            
            $menuItemParams = json_decode($issetMenuItem->params);
            $apiUrl = isset($menuItemParams->host_url)?$menuItemParams->host_url:'';
            $apiKey = isset($menuItemParams->api_key)?$menuItemParams->api_key:'';
            $apiVersion = isset($menuItemParams->api_version)?$menuItemParams->api_version:'';
            
            // get showcase api url
            $apiShowcaseUrl = self::$apiShowcaseUrl;
            // buil link api tours
            $apiShowcaseUrl = self::_buildLink( $apiShowcaseUrl, array('{api_version}', '{api_key}'), array($apiVersion, $apiKey), $apiUrl );
            // connect and get json data of atom-s
            @$items = json_decode(file_get_contents($apiShowcaseUrl));
            
            // parse dates for showcase
            if( !empty((array) $items) ) {
                
                $data->tours = array();
                $data->minDate = 0;
                $data->maxDate = 0;
                self::$_format = 'm.d.Y H:i';
                
                foreach( $items->tour_schedules as $tour ) {
                    
                    if( !empty($tour->tour->schedule) ) {
                        foreach($tour->tour->schedule as $schedule ) {
                            
                            $schedule->date = self::_buildDate($schedule->date);
                            
                            if( $data->minDate == 0 ) 
                            {
                                $data->minDate = $schedule->date;
                            }
                            else if( strtotime($data->minDate) > strtotime($schedule->date) ) 
                            {
                                $data->minDate = $schedule->date;
                            }
                            
                            if( strtotime($data->maxDate) < strtotime($schedule->date) ) 
                            {
                                $data->maxDate = $schedule->date;
                            }
                            
                            $data->tours[$schedule->date][$schedule->time][] = array('name' => $tour->tour->name, 'alias' => $tour->tour->slug);
                            
                            if( count($data->tours[$schedule->date]) > 1 ) uksort($data->tours[$schedule->date], array(self, "_sort")); 
                                                      
                        }
                        
                    }
                    
                }
                
                uksort($data->tours, array(self, "_sort")); 
                
                // filling the days of the week
                $data->tours = self::_fillDates($data->tours, $data->minDate, $data->maxDate);
                
            }
            
        }
        catch( Exception $e )
        {
            return JError::raiseError(404, $e->getMessage());
        }
        
        return $data;
        
    }
      
    /**
     * 
     * @return object
     * 
     * @since  1.0.0 
     * 
     */
    static private function _fillDates( $data = array(), $firstDate = '', $endDate = '' ) {
        
        $tmpDates = array();
        
        $dayWeekStart = date('N', strtotime($firstDate) );
        $dayWeekEnd = date('N', strtotime($endDate) );
        
        // fill start dates
        for( $i = ($dayWeekStart-1); $i > 0; $i-- ) {
            $tmpDates[date('d.m.Y', strtotime($firstDate . " - " . $i ." day"))] = array();
        }
        $data = array_merge( $tmpDates, $data );
        $tmpDates = array();
        
        // fill end dates
        for( $i = $dayWeekEnd; $i < 7; $i++ ) {
            $endDate = date('d.m.Y', strtotime($endDate . " + 1 day"));
            $tmpDates[$endDate] = array();
        }
        $data = array_merge( $data, $tmpDates );
        $tmpDates = array();
        
        $keys = array_keys($data);
        
        foreach( $keys as $k => $date ) {
            
            $index = array_search($date, array_keys($data));
            while( strtotime($date) < strtotime($keys[$k+1]) ) {
                
                $date = date('d.m.Y', strtotime($date . " + 1 day"));
                $tmpDates[$date] = (!isset($data[$date])) ? array() : $data[$date];
                                
            }
            
            $data = array_slice($data, 0, ($index+1), true) + $tmpDates + array_slice($data, ($index), count($data), true);
            $tmpDates = array();
            
        }
        
        return $data;
        
    }  
      
      
    /**
     * 
     * The Method build correct date
     * 
     * @return string
     * 
     * @since  1.0.0 
     */
    static private function _buildDate( $date = '' ) {
        
        $tmpDate = array();
        $tmpDate = explode(".", $date);
        
        $d = $tmpDate[0];
        $m = $tmpDate[1];
        $y = $tmpDate[2];
        
        $d = DateTime::createFromFormat('d', $d)->format('d');
        $m = DateTime::createFromFormat('m', $m)->format('m');
        $y = DateTime::createFromFormat('y', $y)->format('Y');
        
        return $d . '.' . $m . '.' . $y;
        
    }
    
    /**
     * 
     * The Method build link, for atom-s
     * 
     * @param   string   $link, link to connect to the system
     * @param   array    $find, search for replaceable parameters in the link
     * @param   array    $replaced, replacement of parameters from $find
     * @param   string   $apiUrl, adding host link
     * @param   boolean  $prefixApi, additional prefix api
     * 
     * @return string
     * 
     * @since  1.0.0
     */
    static private function _buildLink( $link = '', $find = array(), $replaced = array(), $apiUrl = '', $prefixApi = true ) {
        
        if( !empty($link) && !empty($find) ) 
        {
            // perse and build link
            foreach($find as $k => $regex ) 
            {
                // replaced
                if( isset($replaced[$k]) && !empty($replaced[$k]) ) 
                {
                   $link = str_ireplace($regex, $replaced[$k], $link);
                }
            }
        }
        
        // add home api url
        if( !empty($apiUrl) ) {
            $apiUrl .= (substr($apiUrl, -1) != '/') ? '/' : '';
            if($prefixApi) $apiUrl .= self::$prefixApiUrl;
            $link = $apiUrl . $link;
        }
        
        return $link;
        
    }
    
    /**
     * 
     * The method sort for date/time
     * 
     * @return int
     * 
     * @since  1.0.0 
     * 
     */
    static private function _sort( $first, $end ) {
        
        $first = ( !empty(self::$_format) ) ? date(self::$_format, strtotime($first)) : strtotime($first);
        $end = ( !empty(self::$_format) ) ? date(self::$_format, strtotime($end)) : strtotime($end);
        
        if ($first == $end)
        {
            return 0;
        }
        else if ($first > $end)
        {
            return 1;
        }
        else 
        {
            return -1;
        }
                
    }
    
    /**
	 * 
     * The method to add cycle2 lib
	 *  
     * @param   \Joomla\Registry\Registry  $params  The module options.
     * 
	 * @return  void
     * 
	 */
    public static function addScripts($params, $module) {
        
        $doc = JFactory::getDocument();
        
        $loadCycle2 = true;
        
        // include jquery
		switch($params->get('cycle2', '2')) {
			// include
            case '1':
				$loadCycle2 = true;
				break;
			// auto search tags and include
            case '2':
                $header = $doc->getHeadData(); 
				foreach($header['scripts'] as $scriptName => $scriptData)
				{
					if(substr_count($scriptName,'/cycle'))
					{
						$loadCycle2 = false;
						break;
					}
				}
			break;
            // don't include
            case '0':
				$loadCycle2 = false;
            // load from cms auto
            default: JHtml::_('jquery.framework');
                break;
		}
            
        if($loadCycle2) {
            $doc->addScript( JUri::base(true) . 'media/' . $module->module . '/js/cycle2.min.js' );
        }
        
    }
    
    /**
     * 
     * Method created link for view
     * 
     * @param  string   $slug, slug for a tour of json
     * @param  array    $view, views component to search
     * @param  array    $search, gluing GET parameters search
     * @param  string   $otherParamsLink, first elem text constant in .ini file, other arguments if sprintf
     * 
     * @return string
     * 
     * @since  1.0.0
     * 
     */
    static function createLink( $slug = '', $view = array('tour'), $search = array(), $otherParamsLink = '' ) {
        
        $url = '';
        
        if( is_array($view) && !empty($view) ) {
            
            $menu = JFactory::getApplication()->getMenu();
            
            $tmpParent = '';
            if( is_array($search) && count($search) == 2 ) {
                $firstKey  = '';
                $secondKey = '';
                list($firstKey, $secondKey) = $search;
                $tmpParent = '&'.$firstKey.'='.$secondKey; 
            }
            
            $itemMenu = $menu->getItems('link', 'index.php?option=com_atoms&view='.$view[0].$tmpParent, true);
            $itemid = '';
            if( !empty($itemMenu) ) 
            {
                $itemid = '&Itemid='.$itemMenu->id; 
            }
            // search item id current active
            else if( isset($menu->getActive()->id) )
            {
                $itemid = '&Itemid='.$menu->getActive()->id;
            }
            
            $tmpSlug = '';
            if( $slug != '' ) {
                $tmpSlug = '&tour='.$slug; 
            }
            
            $url = JRoute::_('index.php?option=com_atoms&view='.((isset($view[1]))?$view[1]:$view[0]).$itemid.$tmpSlug.$otherParamsLink, false);
            
        } 
        
        return $url;
        
    } 
    
}